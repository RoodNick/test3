import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductDetailComponent} from './components/product-detail/product-detail.component';
import {ProductSearchComponent} from './components/product-search/product-search.component';

const routes: Routes = [
  {path: '', redirectTo: '/search-product', pathMatch: 'full'},
  {path: 'search-product', component: ProductSearchComponent},
  {path: 'product-detail/:id', component: ProductDetailComponent},
  {path: '**', redirectTo: '/search-product'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
