import {Component, Input} from '@angular/core';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
    selector: 'product-result',
    templateUrl: './product-result.component.html',
    styleUrls: ['./product-result.component.styl']
})
export class ProductResultComponent {
    @Input() productName: string;
    @Input() groupedProducts: Array<{ key: any, value: any }>;

    constructor() {
    }
}
