import {Component, OnInit} from '@angular/core';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {Product} from '../../models/product';
import {ProductTransService} from '../../services/product-trans.service';

@Component({
    selector: 'product-search',
    templateUrl: './product-search.component.html',
    styleUrls: ['./product-search.component.styl']
})
export class ProductSearchComponent implements OnInit {
    productName: string;
    lastProductName: string = '';
    notFound = false;
    groupedProducts: Array<{ key: any, value: any }>;

    constructor(private productTransService: ProductTransService) {
    }

    ngOnInit(): void {
        this.productTransService.searchProducts$.subscribe(
            products => this.groupedProducts = this.groupArrayBy(products)
        );
    }

    search(product: string): void {
        product = product.trim();
        if (product !== this.lastProductName) {
            this.lastProductName = product;
            if (product) {
                this.productTransService.search(product.trim());
            }
        }
    }

    groupArrayBy(products: Product[]): Array<{ key: any, value: any }> {
        const map = new Map();
        products.forEach((product) => {
            const key = product['category'];
            if (!map.has(key)) {
                map.set(key, [product]);
            } else {
                map.get(key).push(product);
            }
        });

        return Array.from(map, x => this.addGroup(x[0], x[1]));
    }

    addGroup(key, value) {
        return {'key': key, 'value': value};
    }
}
