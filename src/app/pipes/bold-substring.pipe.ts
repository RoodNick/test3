import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'substrBold'})
export class SubstringBoldPipe implements PipeTransform {
    transform(value: string, substring: string) {
        let index = value.indexOf(substring);
        if (index > -1) {
            return value.substring(0, index) + `<b>${value.substr(index, substring.length)}</b>` + value.substring(index + substring.length);
        }
        return value;
    }
}