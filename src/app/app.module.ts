import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './components/app.component';
import {ProductDetailComponent} from './components/product-detail/product-detail.component';
import {ProductSearchComponent} from './components/product-search/product-search.component';
import {ProductResultComponent} from './components/product-result/product-result.component';
import {ProductService, ProductTransService} from './services';
import {SubstringBoldPipe} from './pipes';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        ProductDetailComponent,
        ProductSearchComponent,
        ProductResultComponent,
        SubstringBoldPipe
    ],
    providers: [ProductService, ProductTransService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
