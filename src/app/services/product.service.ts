import {Injectable} from '@angular/core';
import {PRODUCTS} from '../models/mock-products';
import {Product} from '../models/product';
import {Http} from '@angular/http';

@Injectable()
export class ProductService {
    constructor() {
    }

    getProducts(): Promise<Product[]> {
        return Promise.resolve(PRODUCTS);
    }

    getProduct(id: number): Promise<Product> {
        return this.getProducts()
            .then(products => products.find(product => product.id === id));
    }

    search(productName: string): Promise<Product[]> {
        return this.getProducts()
            .then(products => products
                .filter(product => product.name.indexOf(productName) > -1)
                );
    }
}
