import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';
import {Product} from '../models/product';
import {ProductService} from './product.service';

@Injectable()
export class ProductTransService {
    private searchProducts = new Subject<string>();

    searchProducts$: Observable<Product[]> = this.searchProducts
    // .debounceTime(300)
        .distinctUntilChanged()
        .switchMap(product => this.productService.search(product));

    constructor(private productService: ProductService) {
    }

    search(product: string): void {
        this.searchProducts.next(product);
    }
}
