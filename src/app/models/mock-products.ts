import { Product } from './product';

export const PRODUCTS: Product[] = [
    { id: 11, name: 'Доска обрезная. Сосна, ель.', category: 'Строительство' },
    { id: 12, name: 'Доска обрезная. Берёза, дуб.', category: 'Строительство' },
    { id: 13, name: 'Доска гладильная 1', category: 'Бытовые' },
    { id: 14, name: 'Доска гладильная 2', category: 'Бытовые' },
    { id: 15, name: 'Доска гладильная 3', category: 'Бытовые' },
    { id: 16, name: 'Мяч футбольный', category: 'Спортивные' },
    { id: 17, name: 'Мяч баскетбольный', category: 'Спортивные' },
    { id: 18, name: 'Мяч волебольный', category: 'Спортивные' },
    { id: 19, name: 'Ракетки для бадминтона', category: 'Спортивные' },
    { id: 20, name: 'Теннисные ракетки', category: 'Спортивные' }
];
